<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String error = request.getAttribute("error") == null ? "404" : (String) request.getAttribute("error");
%>
<jsp:include page="header.jsp"></jsp:include>
<h1>
	Oops
	<%=error%>
</h1>
<h2>Erro na aplicação.</h2>

<jsp:include page="footer.jsp"></jsp:include>
