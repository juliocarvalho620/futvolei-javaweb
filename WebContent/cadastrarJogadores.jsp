<%@page import="Modelo.ApoioModelo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>

<%
	ApoioModelo a = (ApoioModelo) request.getSession().getAttribute("apoio");
	if (a == null) {
		response.sendRedirect("login.jsp");
	}
	
	boolean resultado = Boolean.parseBoolean(request.getParameter("resposta"));
	
	if(resultado){
		out.print("<span>Jogador cadastro com sucesso.</span>");
	}
%>
<div class="containerJogadores">
	<form action="apoio?acao=jogador" method="post">
		<input type="text" class="form-control" name="nome" placeholder="Digite o nome do jogador"><br/>
		<input type="text" class="form-control" name="selecao" placeholder="Digite a seleção"><br/>
		<input type="text" class="form-control" name="numero" placeholder="Digite o número do jogador"> <br/>
		<input type="text" class="form-control" name="cpf" placeholder="Digite o cpf do jogador"> <br/>
		<input type="submit" name="enviar" value="enviar" />
	<a href="dashboardApoio.jsp">Voltar</a>
	</form>
</div>

<jsp:include page="footer.jsp"></jsp:include>