<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<jsp:include page="header.jsp"></jsp:include>

<!-- ======= In�cio ======= -->
	<section id="hero"
		class="d-flex flex-column justify-content-center align-items-center">
		<div class="container" data-aos="fade-in">
			<h1>Bem-Vindo ao FFC</h1>
			<h2>Federa��o De Futv�lei Cearense. Somos uma equipe que
				proporciona para todos um bom conteudo sobre Futv�lei. Aqui voc�
				encontra a melhor plataforma para agendamento e acompanha os
				melhores resultados de seu time.</h2>
			<div class="d-flex align-items-center">
				<i class="bx bxs-right-arrow-alt get-started-icon"></i> <a
					href="#sobre" class="btn-get-started scrollto">Get Started</a>
			</div>
		</div>
	</section>
	<!-- in�cio -->
	
	<main id="main">

		<!-- ======= inicio2 ======= -->
		<section id="why-us" class="why-us">
			<div class="container">

				<div class="row">
					<div class="col-xl-4 col-lg-5" data-aos="fade-up">
						<div class="content">
							<h3>Por que usar o nosso Site?</h3>
							<p>Disponibilizamos sempre conte�dos atualizados para
								saciar a vontade de estar sempre conectado com o esporte
								di�rio. Logue-se ou cadastre-se!</p>
						</div>
					</div>
					<div class="col-xl-8 col-lg-7 d-flex">
						<div class="icon-boxes d-flex flex-column justify-content-center">
							<div class="row">
								<div class="col-xl-4 d-flex align-items-stretch"
									data-aos="fade-up" data-aos-delay="100">
									<div class="icon-box mt-4 mt-xl-0">
										<i class="bx bx-receipt"></i>
										<h4>Logue-se!</h4>
										<p>Fa�a seu login para acompanhar os cont�udos postados
											e os placares de cada rodada. Fa�a t�mbem seus agendamentos de jogos por
											aqui no nosso website.</p>
									</div>
								</div>
								<div class="col-xl-4 d-flex align-items-stretch"
									data-aos="fade-up" data-aos-delay="200">
									<div class="icon-box mt-4 mt-xl-0">
										<i class="bx bx-cube-alt"></i>
										<h4>Cadastre-se!</h4>
										<p>Para fazer seu login, primeiro cadastre-se, para que
											assim possa ter seu acesso!</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- inicio2 -->

		<!-- ======= sobre ======= -->
		<section id="sobre" class="about section-bg">
			<div class="container">

				<div class="row">
					<div
						class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch"
						data-aos="fade-right"></div>

					<div
						class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
						<h3 data-aos="fade-up">Sobre a plataforma</h3>

						<div class="icon-box" data-aos="fade-up">
							<div class="icon">
								<i class="bx bx-fingerprint"></i>
							</div>
							<h4 class="title">
								<a href="">Agendamento</a>
							</h4>
							<p class="description">Cadastre-se para agendar</p>
						</div>

						<div class="icon-box" data-aos="fade-up" data-aos-delay="100">
							<div class="icon">
								<i class="bx bx-gift"></i>
							</div>
							<h4 class="title">
								<a href="">Pontua��o</a>
							</h4>
							<p class="description">agende  uma partida para visualizar os
								pontos.</p>
						</div>

						<div class="icon-box" data-aos="fade-up" data-aos-delay="200">
							<div class="icon">
								<i class="bx bx-atom"></i>
							</div>
							<h4 class="title">
								<a href="">Divirta-se!</a>
							</h4>
							<p class="description">Divirta-se na nossa plataforma.</p>
						</div>

					</div>
				</div>

			</div>
		</section>



		<!-- ======= Esporte ======= -->
		<section id="values" class="values">
			<div class="container">
				<div style="align-text: center; position: relative; left: 35%;">
					<h3>
						<b>Curiosidades sobre o Esporte</b>
					</h3>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
						<div class="card"
							style="background-image: url(assets/img/cap.jpg);">
							<div class="card-body">
								<p class="card-text">Voc� sabia que o futv�lei foi criado
									no Rio de Janeiro na d�cada de 60, e desde o seu inicio tem
									sido um dos esportes de areia que mais cresce em todo pa�s.</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 d-flex align-items-stretch mt-4 mt-md-0"
						data-aos="fade-up" data-aos-delay="100">
						<div class="card"
							style="background-image: url(assets/img/ca.jpg);">
							<div class="card-body">
								<p class="card-text">
									Jogado com uma bola pr�pia para a pratica do esporte, as disputas
									podem acontecer de forma: individual,em duplas, trios ou quartetos, tanto no masculino quanto no
									feminino, podendo ter a possibilidade de ser times <b>mistos</b>.
								</p>
							</div>
						</div>

					</div>
					<div class="col-md-6 d-flex align-items-stretch mt-4"
						data-aos="fade-up" data-aos-delay="200">
						<div class="card" style="background-image: url(assets/img/c.jpg);">
							<div class="card-body">
								<p class="card-text">O futv�lei foi criado com o intuito
					                de burlar uma regra, que proibia nas praias as
									pr�ticas de esportes sem rede e sem espa�o delimitado.</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 d-flex align-items-stretch mt-4"
						data-aos="fade-up" data-aos-delay="300">
						<div class="card"
							style="background-image: url(assets/img/capa.jpg);">
							<div class="card-body">
								<p class="card-text">O futv�lei � uma das modalidades
									que mais faz sucesso nas Areias. Fique atento e n�o perca nehuma novidade.</p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		<!-- Esporte -->
		<!-- ======= servi�os ======= 
		<section id="servicos" class="services section-bg">
			<div class="container">

				<div class="section-title" data-aos="fade-up">
					<h2>Servi�os</h2>
					<p>Veja alguns dos nossos servi�os.</p>
				</div>

				<div class="row">
					<div class="col-lg-4 col-md-6" data-aos="fade-up">
						<div class="icon-box">
							<div class="icon">
								<i class="icofont-computer"></i>
							</div>
							<h4 class="title">
								<a href="">Vers�o</a>
							</h4>
							<p class="description">Existe a vers�o do nosso site para
								mobile e web</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6" data-aos="fade-up"
						data-aos-delay="100">
						<div class="icon-box">
							<div class="icon">
								<i class="icofont-chart-bar-graph"></i>
							</div>
							<h4 class="title">
								<a href="">Sistema</a>
							</h4>
							<p class="description">Salvamos todas as informa��es
								utilizadas no sistema</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6" data-aos="fade-up"
						data-aos-delay="200">
						<div class="icon-box">
							<div class="icon">
								<i class="icofont-earth"></i>
							</div>
							<h4 class="title">
								<a href="">Agendar</a>
							</h4>
							<p class="description">Agende suas partidas e hor�rios por
								aqui</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6" data-aos="fade-up"
						data-aos-delay="300">
						<div class="icon-box">
							<div class="icon">
								<i class="icofont-image"></i>
							</div>
							<h4 class="title">
								<a href="">Log</a>
							</h4>
							<p class="description">Logue-se para ter acesso</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6" data-aos="fade-up"
						data-aos-delay="400">
						<div class="icon-box">
							<div class="icon">
								<i class="icofont-settings"></i>
							</div>
							<h4 class="title">
								<a href="">Infor</a>
							</h4>
							<p class="description">Ao se cadastrar as informa��es
								ficar�o salvas no sistema</p>
						</div>
					</div>
					<div class="col-lg-4 col-md-6" data-aos="fade-up"
						data-aos-delay="500">
						<div class="icon-box">
							<div class="icon">
								<i class="icofont-tasks-alt"></i>
							</div>
							<h4 class="title">
								<a href="">Relat�rio</a>
							</h4>
							<p class="description">Fazemos um resumo das partidas
								motrando os maiores pontuadores</p>
						</div>
					</div>
				</div>

			</div>
		</section>
		servi�os -->
		<!-- ======= Atletas======= -->
		<section style="background-image: url(assets/img/no.PNG);"
			id="testimonials" class="testimonials">
			<div class="container" data-aos="fade-up">

				<div class="owl-carousel testimonials-carousel">

					<div class="testimonial-item">
						<img src="assets/img/vinicius.jpg" class="testimonial-img" alt="">
						<h3>Vinicius Souza</h3>
						<h4>Vinicius Ferreira de Sousa</h4>
						<p>
							<i class="bx bxs-quote-alt-left quote-icon-left"></i> Sim, eu
							jogava futebol, essa paix�o surgiu por influ�ncia do meu pai,
							quando ele parou de jogar futebol e come�ou a jogar futev�lei
							por lazer e eu sempre fui muito parceiro dele <i
								class="bx bxs-quote-alt-right quote-icon-right"></i>
						</p>
					</div>

					<div class="testimonial-item">
						<img src="assets/img/p.jpg" class="testimonial-img" alt="">
						<h3>Pedrinho</h3>
						<h4>Pedro Victor Delmino Da Silva</h4>
						<p>
							<i class="bx bxs-quote-alt-left quote-icon-left"></i> O Gabriel
					          Silva "rato", estava jogando em Miami. Mas nosso time (favorito), se
							eu falar que ele leva vantagem vai parecer que � desculpa. Pode
							falar pra ele que eu vou atropelar (risos) <i
								class="bx bxs-quote-alt-right quote-icon-right"></i>
						</p>
					</div>

					<div class="testimonial-item">
						<img src="assets/img/n.jpg" class="testimonial-img" alt="">
						<h3>Nat�lia</h3>
						<h4>Nat�lia Guitler</h4>
						<p>
							<i class="bx bxs-quote-alt-left quote-icon-left"></i> Fui tenista
							profissional por 5 anos e morei na Argentina jogando
							profissionalmente. Cheguei atingir as 450 do mundo. Antes do
							t�nis, ainda muito novinha, tamb�m joguei futebol. Ent�o,
							foi uma mistura de esportes que acabou me levando para o
							futev�lei. <i class="bx bxs-quote-alt-right quote-icon-right"></i>
						</p>
					</div>

					<div class="testimonial-item">
						<img src="assets/img/and.jpg" class="testimonial-img" alt="">
						<h3>Anderson</h3>
						<h4>Anderson �guia</h4>
						<p>
							<i class="bx bxs-quote-alt-left quote-icon-left"></i> O
							futv�lei cresce independentemente de incentivo, embora eu ache
							que ainda est� muito fraco.Se n�o fosse isso, j� seriamos
							uma modalidade olimpica. Agora, a gente est� caminhando <i
								class="bx bxs-quote-alt-right quote-icon-right"></i>
						</p>
					</div>

					<div class="testimonial-item">
						<img src="assets/img/anp.jpg" class="testimonial-img" alt="">
						<h3>Paulinha</h3>
						<h4>Ana Paula Costa</h4>
						<p>
							<i class="bx bxs-quote-alt-left quote-icon-left"></i> Gostaria de
							agradecer aos meus treinadores e atletas da Life Futv�lei,
							Iury Lima e Gustavo Vieira pelos os ensinamentos e por me mostrar
							os atalhos. A imprensa feirense em especial ao Grupo Folha do
							Estado, que tem sempre nos apoiado e a todos que apostam e
							acreditam em nosso potencial <i
								class="bx bxs-quote-alt-right quote-icon-right"></i>
						</p>
					</div>

				</div>

			</div>
		</section>
		<!-- Atletas -->

		<!-- ======= Regras ======= -->
		<section id="team" class="team section-bg">
			<div class="container">

				<div class="section-title">
					<h2 data-aos="fade-up">Regras</h2>
					<p data-aos="fade-up">Para voc� e seus amigos praticarem o
						esporte sem duvidas do que pode ou n�o pode nesse esporte
					     brasileiro!</p>
				</div>

				<div class="row">

					<div class="col-lg-3 col-md-6 d-flex align-items-stretch"
						data-aos="fade-up">
						<div class="member">
							<div class="member-img">
								<img style="width: 100%; height: 271px;" src="assets/img/l.jpg"
									class="img-fluid" alt="">
								<div class="social">
									<a href=""><i class="icofont-twitter"></i></a> <a href=""><i
										class="icofont-facebook"></i></a> <a href=""><i
										class="icofont-instagram"></i></a> <a href=""><i
										class="icofont-linkedin"></i></a>
								</div>
							</div>
							<div class="member-info">
								<h4>Toques</h4>
								<span>Durante uma partida, a dupla s� pode dar no
									maximo tr�s toques na bola.</span>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 d-flex align-items-stretch"
						data-aos="fade-up" data-aos-delay="100">
						<div class="member">
							<div class="member-img">
								<img style="width: 100%; height: 271px;" src="assets/img/w.jpg"
									class="img-fluid" alt="">
								<div class="social">
									<a href=""><i class="icofont-twitter"></i></a> <a href=""><i
										class="icofont-facebook"></i></a> <a href=""><i
										class="icofont-instagram"></i></a> <a href=""><i
										class="icofont-linkedin"></i></a>
								</div>
							</div>
							<div class="member-info">
								<h4>Dois Toques</h4>
								<span>Se o jogador der dois toques na bola � erro e
									ponto para a dupla advers�ria.</span>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 d-flex align-items-stretch"
						data-aos="fade-up" data-aos-delay="200">
						<div class="member">
							<div class="member-img">
								<img style="width: 100%; height: 271px;" src="assets/img/i.jpg"
									class="img-fluid" alt="">
								<div class="social">
									<a href=""><i class="icofont-twitter"></i></a> <a href=""><i
										class="icofont-facebook"></i></a> <a href=""><i
										class="icofont-instagram"></i></a> <a href=""><i
										class="icofont-linkedin"></i></a>
								</div>
							</div>
							<div class="member-info">
								<h4>Saque</h4>
								<span>O monte do saque fica fora da quadra atr�s da
									linha da marca��o.</span>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 d-flex align-items-stretch"
						data-aos="fade-up" data-aos-delay="300">
						<div class="member">
							<div class="member-img">
								<img style="width: 100%; height: 271px;" src="assets/img/z.jpg"
									class="img-fluid" alt="">
								<div class="social">
									<a href=""><i class="icofont-twitter"></i></a> <a href=""><i
										class="icofont-facebook"></i></a> <a href=""><i
										class="icofont-instagram"></i></a> <a href=""><i
										class="icofont-linkedin"></i></a>
								</div>
							</div>
							<div class="member-info">
								<h4>Rede</h4>
								<span>Se o jogador tocar na rede � erro e ponto para a
									dupla advers�ria.</span>
							</div>
						</div>
					</div>

				</div>

			</div>
		</section>
		<!-- Regras-->

		<!-- ======= resumo ======= -->
		<section id="resumo" class="pricing">
			<div class="container">

				<div class="section-title">
					<h2 data-aos="fade-up">Resumo</h2>
					<p data-aos="fade-up">Aqui voc� acompanha os tr�s ultimos
						melhores jogadores!</p>
				</div>

				<div class="row" align="center">

					<div class="col-lg-3 col-md-6" data-aos="fade-up">
						<div class="box">
							<h3>1� Coloca��o</h3>
							<ul>
								<li>Guilherme Campelo</li>
								<li>790</li>
							</ul>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 mt-4 mt-lg-0" data-aos="fade-up"
						data-aos-delay="200">
						<div class="box">
							<h3>2� Coloca��o</h3>
							<ul>
								<li>Willian Mussi</li>
								<li>670</li>
								
							</ul>
						</div>
					</div>


					<div style="position: center center"
						class="col-lg-3 col-md-6 mt-4 mt-lg-0" data-aos="fade-up"
						data-aos-delay="300">
						<div class="box">
							<h3>3� Coloca��o</h3>
							<ul>
								<li>Ricardo Massigna</li>
								<li>540</li>
							
							</ul>
						</div>
					</div>

				</div>

			</div>


		</section>
		<!-- resumo -->

		<!-- ======= F.A.Q Section ======= -->
		<section id="faq" class="faq section-bg">
			<div class="container">

				<div class="section-title">
					<h2 data-aos="fade-up">Sobre o Esporte:</h2>
					<p data-aos="fade-up">Modalidade de esporte de areia pr�ticada
						em quadras montadas nas orlas</p>
				</div>

				<div class="faq-list">
					<ul>
						<li data-aos="fade-up"><i class="bx bx-help-circle icon-help"></i>
							<a data-toggle="collapse" class="collapse" href="#faq-list-1">Originalidade<i
								class="bx bx-chevron-down icon-show"></i><i
								class="bx bx-chevron-up icon-close"></i></a>
							<div id="faq-list-1" class="collapse show"
								data-parent=".faq-list">
								<p>O esporte foi originado nas praias do Rio de Janeiro por
									volta de 1960 e, ao longo do tempo, cresceu dentro do Brasil,
									assim como na Europa, na �sia e nos Estados Unidos.</p>
							</div></li>

						<li data-aos="fade-up" data-aos-delay="100"><i
							class="bx bx-help-circle icon-help"></i> <a
							data-toggle="collapse" href="#faq-list-2" class="collapsed">Sistema
								Do Jogo<i class="bx bx-chevron-down icon-show"></i><i
								class="bx bx-chevron-up icon-close"></i>
						</a>
							<div id="faq-list-2" class="collapse" data-parent=".faq-list">
								<p>� jogado em sistemas de duplas (2x2), trios (3x3)[3] ou
									quartetos (4x4)[4] masculinos, femininos ou mistos. Deve-se
									tocar a bola com qualquer parte do corpo, exceto os bra�os,
									antebra�os e as m�os como no futebol.</p>
							</div></li>

						<li data-aos="fade-up" data-aos-delay="200"><i
							class="bx bx-help-circle icon-help"></i> <a
							data-toggle="collapse" href="#faq-list-3" class="collapsed">Disputa<i
								class="bx bx-chevron-down icon-show"></i><i
								class="bx bx-chevron-up icon-close"></i></a>
							<div id="faq-list-3" class="collapse" data-parent=".faq-list">
								<p>O jogo � disputado, como no voleibol, em disputa de sets
									(de 18 pontos sem vantagem) na qual os pontos s�o marcados
									quando a bola cai na quadra do adversario, ou quando a bola � desviada para
									fora das quadras pelos mesmos, ou bate em qualquer outro jogador do time advers�rio
									ao tocar no ch�o.</p>
							</div></li>

						<li data-aos="fade-up" data-aos-delay="300"><i
							class="bx bx-help-circle icon-help"></i> <a
							data-toggle="collapse" href="#faq-list-4" class="collapsed">Hist�ria<i
								class="bx bx-chevron-down icon-show"></i><i
								class="bx bx-chevron-up icon-close"></i></a>
							<div id="faq-list-4" class="collapse" data-parent=".faq-list">
								<p>O futev�lei chegou em Portugal no seguimento da
									expans�o que se verificou a n�vel mundial, mais
									concretamente � Cidade da P�voa de Varzim no final da
									d�cada de 80, local onde se registaram os primeiros encontros
									regulares entre adeptos da modalidade.</p>
							</div></li>

						<li data-aos="fade-up" data-aos-delay="400"><i
							class="bx bx-help-circle icon-help"></i> <a
							data-toggle="collapse" href="#faq-list-5" class="collapsed">Campeonatos
								<i class="bx bx-chevron-down icon-show"></i><i
								class="bx bx-chevron-up icon-close"></i>
						</a>
							<div id="faq-list-5" class="collapse" data-parent=".faq-list">
								<p>A primeira etapa do Campeonato Mundial de Futev�lei de
									2010 ocorreu em Salvador nos dias 6 e 7 de mar�o, o qual
									sucedeu a 1� Etapa do Circuito Brasileiro de Futev�lei
									Masculino tamb�m na capital baiana.</p>
							</div></li>

					</ul>
				</div>

			</div>
		</section>
		<!-- End F.A.Q Section -->

		<!-- ======= agendamento Section ======= -->
		<section id="agendamento" class="agendamento">
			<div class="container">

				<div class="section-title">
					<h2 data-aos="fade-up">Agendamento Dos Jogos</h2>
					<p data-aos="fade-up">Agende aqui os seus jogos!</p>
				</div>

				<div class="row justify-content-center" data-aos="fade-up"
					data-aos-delay="300">
					<div class="col-xl-9 col-lg-12 mt-4">
						<form action="forms/agendamento.php" method="post" role="form"
							class="php-email-form">
							<div class="form-row">
								<div class="col-md-6 form-group">
									<input type="text" name="name" class="form-control" id="name"
										placeholder="Digite aqui o seu nome (responsavel)"
										data-rule="minlen:4"
										data-msg="Digite no m�nimo 4 caracteres!" />
									<div class="validate"></div>
								</div>
								<div class="col-md-6 form-group">
									<input type="date" name="name" class="form-control" id="name"
										placeholder="Data do seu nascimento. Modo: DD/MM/AAAA"
										data-rule="minlen:4"
										data-msg="Digite no m�nimo 4 caracteres!" />
									<div class="validate"></div>
								</div>
								<br> <br>
								<p></p>
								<div style="left: 25%;" class="col-md-6 form-group">
									<div class="dropdown">
										<button style="width: 186px;" class="dropbtn">Dia Da
											Semana</button>
										<div style="left: 7%; text-align: center;"
											class="dropdown-content">
											<a href="#">Segunda</a> <a href="#">Ter�a</a> <a href="#">Quarta</a>
											<a href="#">Quinta</a> <a href="#">Sexta</a> <a href="#">S�bado</a>
										</div>
									</div>
									<div class="dropdown">
										<button style="width: 186px;" class="dropbtn">Hor�rios</button>
										<div style="left: 7%; text-align: center;"
											class="dropdown-content">
											<a href="#">hor�rio 1</a> <a href="#">hor�rio 2</a> <a
												href="#">hor�rio 3</a>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="dropdown">
									<button
										style="width: 375px; align: center; position: relative; left: 57%;"
										class="dropbtn">N�mero De Pessoas</button>
									<div style="width: 375px; text-align: center; left: 57%;"
										class="dropdown-content">
										<a href="#">1 a 6 pessoas</a> <a href="#">7 a 12 pessoas</a> <a
											href="#">13 a 18 pessoas</a> <a href="#">19 a 24 pessoas</a>
									</div>
								</div>
								<br>
							</div>
							<div class="form-group">
								<textarea class="form-control" name="message" rows="5"
									data-rule="required"
									data-msg="Digite algo. Ex.: n�mero de pessoas"
									placeholder="Observa��o"></textarea>
								<div class="validate"></div>
							</div>
							<div class="text-center">
								<button type="submit"
									style="background-color: #ff5821; color: white;">Send
									Message</button>
							</div>
						</form>
					</div>

				</div>

			</div>
		</section>
		<!-- End agendamento Section -->

	</main>
	<!-- End #main -->
<jsp:include page="footer.jsp"></jsp:include>