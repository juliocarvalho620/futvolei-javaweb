<%@page import="Modelo.ApoioModelo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>

<center><h1>Login apoio</h1></center>
<%
	String error = (String) request.getAttribute("error");
int codigoAcesso = request.getAttribute("codigoAcesso") == null ? -1 : (int) request.getAttribute("codigoAcesso");

ApoioModelo a = (ApoioModelo) request.getSession().getAttribute("apoio");
if (a != null) {
	response.sendRedirect("dashboardApoio.jsp");
}
%>

<br><center><form action="apoio?acao=entrar" method="post">
	<%
		if (error != null) {
		out.print("<span>" + error + "</span>");

	}

	if (codigoAcesso != -1) {
		if (codigoAcesso > 0) {
			out.print("<span>Seu codigo de acesso é: " + codigoAcesso + "<span>");
		}
	}
	%>
	<input name="codigo" type="text" value="entrar"
		placeholder="digite seu codigo de acesso"/><br> <input name="senha"
		type="password" placeholder="digite sua senha"/> <input
		name="entrar" type="submit" value="entrar"/>
	<p>
		<br><a href="apoio.jsp">Cadastrar apoio</a><br>
	</p>
</form></center>
<jsp:include page="footer.jsp"></jsp:include>
