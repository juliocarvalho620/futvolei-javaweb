<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="Modelo.ApoioModelo"%>
<%
	ApoioModelo a = (ApoioModelo) request.getSession().getAttribute("apoio");
%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>FFC</title>
<meta content="" name="descriptison">
<meta content="" name="keywords">


<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="assets/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
<link href="assets/vendor/boxicons/css/boxicons.min.css"
	rel="stylesheet">
<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	rel="stylesheet">
<link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="assets/css/style.css" rel="stylesheet">
<!-- mystyle -->
<link href="assets/css/mystyle.css" rel="stylesheet">

<style type="text/css">
.dropbtn {
	background-color: #FF511F;
	color: white;
	padding: 9px;
	font-size: 16px;
	border: none;
}

.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f1f1f1;
	min-width: 160px;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
}

.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
}

.dropdown-content a:hover {
	background-color: #ddd;
}

.dropdown:hover .dropdown-content {
	display: block;
}

.dropdown:hover .dropbtn {
	background-color: #FAAD81;
}
</style>
</head>

<body>

	<!-- ======= Header ======= -->
	<header id="header">
		<div class="container d-flex">

			<div class="logo mr-auto">
				<h1 class="text-light">
					<a href="index.jsp"><span>FFC</span></a>
				</h1>
			</div>

			<nav class="nav-menu d-none d-lg-block">
				<ul>
					<li class="active"><a href="index.jsp">Início</a></li>
					<li><a href="#sobre">Sobre</a></li>
					<!-- <li><a href="#servicos">Serviços</a></li> -->
					<li><a href="#resumo">Resumo</a></li>
					<li><a href="#agendamento">Agendamento</a></li>
					
					<%
						if (a == null) {
							out.print("<li><a href='login.jsp'>Entrar (apoio)</a></li>");
						}else{
							out.print("<li><a href='home?acao=logout'>logout</a></li>");
						}
					%>
				</ul>
			</nav>
			<!-- .nav-menu -->

		</div>
	</header>
	<!--Header -->