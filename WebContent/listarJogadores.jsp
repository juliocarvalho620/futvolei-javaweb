<%@page import="Modelo.ApoioModelo"%>
<%@page import="Controle.JogadorController"%>
<%@page import="Modelo.Jogador"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>

<%
	ApoioModelo a = (ApoioModelo) request.getSession().getAttribute("apoio");
	int id_apoio = 0;
	
	if(a == null){
		response.sendRedirect("index.jsp");
	}else{
		 id_apoio = a.getId();
	}
	
	ArrayList<Jogador> jg = new JogadorController().consultar(id_apoio);
	
	if(jg.size() > 0){
	
		ArrayList<String> selecoes = new ArrayList<String>();
	
		//separar selecoes
		for (Jogador j : jg) {
			if (selecoes.indexOf(j.getSelecao()) == -1) {
				selecoes.add(j.getSelecao());
			}
		}
%>
<a href="dashboardApoio.jsp">voltar</a>
<%
	for (String selecao : selecoes) {
		out.print("<h3>Seleção: "+selecao+"</h3>");
		out.print("<table>");
		out.print("<thead>");
		out.print("<tr>");
		out.print("<th>NOME</th>");
		out.print("<th>CPF</th>");
		out.print("<th>NUMERO</th>");
		out.print("<th>EDITAR</th>");
		out.print("<th>DELETAR</th>");
		out.print("</tr>");
		out.print("</thead>");

		for (Jogador j : jg) {
			if (j.getSelecao().equals(selecao)) {
				out.print("<tr>");
				out.print("<td>" + j.getNome() + "</td>");
				out.print("<td>" + j.getCpf() + "</td>");
				out.print("<td>" + j.getNumero() + "</td>");
				out.print("<td><a href='editarJogadores.jsp?id="+j.getId()+"'>editar</a><td>");
				out.print("<td><a href='apoio?acao=deletar&id="+j.getId()+"'>deletar</a><td>");
				out.print("</tr>");
			}

		}
		out.print("</table>");
		out.print("<br/>");
	}
}else{
	out.print("<h3>Nenhum jogador cadastrado.</h3>");
	out.print("<a href='dashboardApoio.jsp'>Voltar</a>");
}
%>
<jsp:include page="footer.jsp"></jsp:include>
