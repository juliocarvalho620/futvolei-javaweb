<%@page import="Controle.JogadorController"%>
<%@page import="Modelo.Jogador"%>
<%@page import="Modelo.ApoioModelo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>

<%
	ApoioModelo a = (ApoioModelo) request.getSession().getAttribute("apoio");
	if (a == null) {
		response.sendRedirect("login.jsp");
	}
	
	boolean resultado = Boolean.parseBoolean(request.getParameter("resposta"));
	if (resultado) {
		out.print("<span>Jogador editado com sucesso.</span>");
	}
	
	int id_jogador = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
	
	Jogador jg = new JogadorController().consultarPorId(id_jogador);
	if(jg != null){
		
%>

<form action="apoio?acao=editarJogador" method="post">
	<input type="hidden" value="<%= jg.getId()%>" name="id"/>
	<input type="text" name="nome" value="<%= jg.getNome()%>" placeholder="Digite o nome do jogador">
	<input type="text" name="selecao" value="<%= jg.getSelecao()%>" placeholder="Digite a seleção">
	<input type="text" name="numero" value="<%= jg.getNumero()%>" placeholder="Digite o número do jogador"> 
	<input type="text" name="cpf" value="<%= jg.getCpf()%>" placeholder="Digite o cpf do jogador"> 
	<input type="submit" name="enviar" value="enviar" />
</form>
<%  
}else{
	response.sendRedirect("listarJogadores.jsp");
}
%>
<a href="listarJogadores.jsp">Voltar</a>

<jsp:include page="footer.jsp"></jsp:include>