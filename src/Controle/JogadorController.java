package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Modelo.Jogador;

public class JogadorController {
	public boolean inserir(Jogador jogador) {
		boolean resultado = false;

		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "INSERT INTO CADASTRO(NOME, SELECAO, NUMERO, CPF, APOIO) VALUES (?,?, ?, ?, ?)";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setString(1, jogador.getNome());
			ps.setString(2, jogador.getSelecao());
			ps.setInt(3, jogador.getNumero());
			ps.setString(4, jogador.getCpf());
			ps.setInt(5, jogador.getApoio());

			if (!ps.execute()) {
				resultado = true;
			}

			new Conexao().fecharConexao(connect);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return resultado;
	}

	public ArrayList<Jogador> consultar(int id_apoio) {
		ArrayList<Jogador> lista = new ArrayList<Jogador>();
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM CADASTRO WHERE APOIO=" + id_apoio;
			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					Jogador jg = new Jogador();
					jg.setApoio(id_apoio);
					jg.setCpf(rs.getString("CPF"));
					jg.setNome(rs.getString("NOME"));
					jg.setNumero(rs.getInt("NUMERO"));
					jg.setSelecao(rs.getString("SELECAO"));
					jg.setId(rs.getInt("ID"));
					lista.add(jg);
				}
			}

			new Conexao().fecharConexao(con);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return lista;
	}

	public Jogador consultarPorId(int id_jogador) {
		Jogador jg = null;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "SELECT * FROM CADASTRO WHERE ID=" + id_jogador;
			PreparedStatement ps = con.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					jg = new Jogador();
					jg.setApoio(id_jogador);
					jg.setCpf(rs.getString("CPF"));
					jg.setNome(rs.getString("NOME"));
					jg.setNumero(rs.getInt("NUMERO"));
					jg.setSelecao(rs.getString("SELECAO"));
					jg.setId(rs.getInt("ID"));
				}
			}
			new Conexao().fecharConexao(con);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return jg;
	}

	public boolean deletar(int id) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "DELETE FROM CADASTRO WHERE id=?;";
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, id);
			int a = ps.executeUpdate();
	
			if (a == 1) {
				resultado = true;
			}
			new Conexao().fecharConexao(connect);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	
	public boolean editar(Jogador jogador) {
		boolean resultado = false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "UPDATE CADASTRO SET NOME = ?, SELECAO = ?, NUMERO = ?, CPF = ? WHERE id=?;";
			PreparedStatement ps = connect.prepareStatement(sql);
			
			ps.setString(1, jogador.getNome());
			ps.setString(2, jogador.getSelecao());
			ps.setInt(3, jogador.getNumero());
			ps.setString(4, jogador.getCpf());
			ps.setInt(5, jogador.getId());

			int jg = ps.executeUpdate();
	
			if (jg == 1) {
				resultado = true;
			}
			new Conexao().fecharConexao(connect);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
}
