package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Modelo.ApoioModelo;

public class ApoioController {
	public int inserir(ApoioModelo apoio) {
		int id = 0;
		
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql = "INSERT INTO APOIO(nome, senha) VALUES (?,?)";
			PreparedStatement ps = connect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, apoio.getNome());
			ps.setString(2, apoio.getSenha());
			
			if (!ps.execute()) {
				ResultSet rs = ps.getGeneratedKeys();
				
				if(rs.next()){
					id = rs.getInt(1);
				}	
				
			}

			new Conexao().fecharConexao(connect);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return id;
	}
	
	public ApoioModelo selecionar(ApoioModelo apoio) {
		ApoioModelo resultado= null;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql="SELECT * FROM APOIO WHERE id=? and senha=?;";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setInt(1, apoio.getId());
			ps.setString(2, apoio.getSenha());
			
			ResultSet rs= ps.executeQuery();
			if(rs.next()){
				resultado = new ApoioModelo();
				resultado.setId(rs.getInt("ID"));
				resultado.setNome(rs.getString("nome"));
				resultado.setSenha(rs.getString("senha"));
			}
			
			new Conexao().fecharConexao(connect);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}

}
