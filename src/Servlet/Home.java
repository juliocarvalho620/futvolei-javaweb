package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Home() {
		super();
	}

	private RequestDispatcher dispacher(String forward) {
		return getServletContext().getRequestDispatcher("/" + forward);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String parameter = request.getParameter("acao") != null ? request.getParameter("acao") : "";

		switch (parameter) {
		case "entrar":
			dispacher("entrar.jsp").forward(request, response);
			break;
		case "logout":
			HttpSession session = request.getSession();
			if(session.getAttribute("apoio") != null) {
				session.invalidate();
				response.sendRedirect("index.jsp");
			}
			break;
		default:
			request.setAttribute("error", "404");
			dispacher("oops.jsp").forward(request, response);
		}
	}

}
