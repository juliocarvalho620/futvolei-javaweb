package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Controle.ApoioController;
import Controle.JogadorController;
import Modelo.ApoioModelo;
import Modelo.Jogador;

@WebServlet("/apoio")
public class Apoio extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Apoio() {
		super();
	}
	
	private RequestDispatcher dispacher(String forward) {
		return getServletContext().getRequestDispatcher("/" + forward);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String parameter = request.getParameter("acao") != null ? request.getParameter("acao") : "";
		switch(parameter) {
		case "deletar":
			doPost(request, response);
			break;
		default:
			request.setAttribute("error", "404");
			dispacher("oops.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String parameter = request.getParameter("acao") != null ? request.getParameter("acao") : "";

		String senha = null;
		String nome = null;
		int codigoAcesso = 0;
		ApoioModelo apoio = null;

		switch (parameter) {
		case "cadastro":
			nome = request.getParameter("nome");
			senha = request.getParameter("senha");

			apoio = new ApoioModelo();
			apoio.setNome(nome);
			apoio.setSenha(senha);

			codigoAcesso = new ApoioController().inserir(apoio);

			if (codigoAcesso != 0) {
				request.setAttribute("codigoAcesso", codigoAcesso);
				dispacher("login.jsp").forward(request, response);
			} else {
				request.setAttribute("codigoAcesso", 0);
				dispacher("apoio.jsp").forward(request, response);
			}

			break;
		case "entrar":

			codigoAcesso = Integer.parseInt(request.getParameter("codigo"));
			senha = request.getParameter("senha");

			apoio = new ApoioModelo();
			apoio.setId(codigoAcesso);
			apoio.setSenha(senha);

			apoio = new ApoioController().selecionar(apoio);

			if (apoio != null) {
				HttpSession session = request.getSession();
				session.setAttribute("apoio", apoio);
				dispacher("dashboardApoio.jsp").forward(request, response);
			} else {
				request.setAttribute("error", "Apoio n�o encontrado.");
				dispacher("login.jsp").forward(request, response);
			}

			break;
		case "jogador":
			Jogador jogador = new Jogador();
			apoio = (ApoioModelo) request.getSession().getAttribute("apoio");

			nome = request.getParameter("nome");
			String selecao = request.getParameter("selecao");
			int numero = Integer.parseInt(request.getParameter("numero"));
			String cpf = request.getParameter("cpf");			
			int id_apoio = apoio.getId();
			
			jogador.setApoio(id_apoio);
			jogador.setNome(nome);
			jogador.setCpf(cpf);
			jogador.setNumero(numero);
			jogador.setSelecao(selecao);
			

			if(new JogadorController().inserir(jogador)) {
				response.sendRedirect("cadastrarJogadores.jsp?resposta=true");
			}else {
				response.sendRedirect("cadastrarJogadores.jsp?resposta=false");
			}
			
			break;
		case "deletar":
			int id =  request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
			Jogador jg = new JogadorController().consultarPorId(id);
			
			if(jg != null) {
				if(new JogadorController().deletar(id)) {
					response.sendRedirect("listarJogadores.jsp");		
				}else {
					response.sendRedirect("listarJogadores.jsp");		
				}
			}else {
				response.sendRedirect("listarJogadores.jsp");
			}
			
			break;
		case "editarJogador":
			Jogador jgr = new Jogador();

			nome = request.getParameter("nome");
			String sele = request.getParameter("selecao");
			int num = Integer.parseInt(request.getParameter("numero"));
			String CPF = request.getParameter("cpf");			
			int id_jogador =  Integer.parseInt(request.getParameter("id"));
			
			jgr.setId(id_jogador);
			jgr.setNome(nome);
			jgr.setCpf(CPF);
			jgr.setNumero(num);
			jgr.setSelecao(sele);
			
			if(new JogadorController().editar(jgr)) {
				response.sendRedirect("editarJogadores.jsp?id="+id_jogador+"&resposta=true");
			}else {
				response.sendRedirect("editarJogadores.jsp?id="+id_jogador);
			}
			break;
		default:
			request.setAttribute("error", "404");
			dispacher("oops.jsp").forward(request, response);
		}
	}

}
