package Modelo;

public class Pontos{
	private int id;
    private int timeA;
    private int timeB;
    
	public int getTimeB() {
		return timeB;
	}
	public void setTimeB(int timeB) {
		this.timeB = timeB;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTimeA() {
		return timeA;
	}
	public void setTimeA(int timeA) {
		this.timeA = timeA;
	}

   
}